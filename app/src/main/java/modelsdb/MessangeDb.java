package modelsdb;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "messanges", id = "_id")
public class MessangeDb extends Model {

    public MessangeDb() {
    }

    @Column (name = "date")
    public String date;

    @Column (name = "isServerMess")
    public int isServerMessange;

    @Column (name = "author_name")
    public String author_name;

    @Column (name = "messanges")
    public String messanges;

    public MessangeDb setIsServerMessange(int isServerMessange) {
        this.isServerMessange = isServerMessange;
        return this;
    }

    public MessangeDb setDate(String date) {
        this.date = date;
        return this;
    }

    public MessangeDb setMessanges(String messanges) {
        this.messanges = messanges;
        return this;
    }

    public MessangeDb setAuthorName(String author_name) {
        this.author_name = author_name;
        return this;
    }

    public static List<MessangeDb> getAll() {
        return new Select()
                .from(MessangeDb.class)
                .orderBy("_id ASC")
                .execute();
    }
}
