package modelsdb;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;


@Table(name = "author", id = "_id")
public class AuthorDb extends Model{

    public AuthorDb() {
    }

    @Column(name = "author_name")
    public String authName;

    @Column(name = "author_pass")
    public String authPass;

    public static List<AuthorDb> getAuth(String author) {
        return new Select()
                .from(AuthorDb.class)
                .where("author_name = ?", author)
                .execute();
    }

}
