package dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.slava.messanger.Client;
import com.example.slava.messanger.ConnectedCallBack;
import com.example.slava.messanger.R;


public class DialogIpEnter extends AlertDialog.Builder {

    Context context;
    Client client;

    public DialogIpEnter(final Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_ip_address_enter, null);
        final EditText edIpAddress = (EditText) dialogView.findViewById(R.id.edIpAdress);
        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.imgClose);

        this.setView(dialogView)
            .setCancelable(false)
            .setPositiveButton(R.string.connect,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, int which) {
                            if (edIpAddress.getText().toString().isEmpty()) {
                                Toast.makeText(
                                        context,
                                        R.string.enter_ip,
                                        Toast.LENGTH_LONG
                                ).show();
                            } else {
                                client = new Client(edIpAddress.getText().toString());
                                client.start();
                                // устанавливает CallBack для проверки удалось ли подклбчится
                                client.setConnectedCallback(new ConnectedCallBack() {
                                    @Override
                                    public void isConnectedCallback(boolean isConnected) {
                                        if (isConnected) {
                                            Log.d("Client", "Connected Callback");
                                            ((Activity) context).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //если удалось подключится открываем диалог для авторизации
                                                    new DialogAuthEnter(
                                                        context,
                                                        R.style.MyAlertDialog,
                                                        client
                                                    );
                                                    dialog.dismiss();
                                                }
                                            });
                                        }
                                    }
                                });

                            }
                        }
                    }
            )
            .setNegativeButton(R.string.cancel,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        final AlertDialog alert = this.create();
        imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.hide();
                }
            });
        alert.show();
    }

}
