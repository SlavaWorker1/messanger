package dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.slava.messanger.ChatActivity;
import com.example.slava.messanger.ChatFragment;
import com.example.slava.messanger.Client;
import com.example.slava.messanger.ConnectedCallBack;
import com.example.slava.messanger.R;
import com.example.slava.messanger.Utils;

import org.json.JSONException;

import java.io.IOException;

import modelsdb.AuthorDb;


public class DialogRegister extends AlertDialog.Builder {

    public DialogRegister(final Context context, int themeResId) {
        super(context, themeResId);
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_register, null);
        final EditText edUserName = (EditText) dialogView.findViewById(R.id.edUsername);
        final EditText edPassword = (EditText) dialogView.findViewById(R.id.edPassword);
        final EditText edConfirmPass = (EditText) dialogView.findViewById(R.id.edConfirmPassword);
        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.imgClose);

        this.setView(dialogView)
                .setCancelable(false)
                .setPositiveButton(R.string.create_acc,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {

                            }
                        }
                )
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = this.create();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.hide();
            }
        });
        alert.show();
        alert.getButton(alert.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( edUserName.getText().toString().isEmpty() ||
                        edPassword.getText().toString().isEmpty() ||
                        edConfirmPass.getText().toString().isEmpty()) {
                    Toast.makeText(
                            context,
                            R.string.enter_all_lines,
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    if (edPassword.getText().toString().equals(edConfirmPass.getText().toString())) {
                        AuthorDb authorDb = new AuthorDb();
                        authorDb.authName = edUserName.getText().toString();
                        authorDb.authPass = edPassword.getText().toString();
                        authorDb.save();
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra(
                                Utils.SERVER,
                                context.getString(R.string.Server)
                        );
                        intent.putExtra("username", edUserName.getText().toString());
                        context.startActivity(intent);
                        alert.dismiss();
                    } else {
                        Toast.makeText(
                                context,
                                R.string.different_passwords,
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }
            }
        });
    }


    /**
     * Перегруженный конструктор для регистрации клиента через сервер
     */
    public DialogRegister(final Context context, int themeResId, final Client client) {
        super(context, themeResId);

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_register, null);
        final EditText edUserName = (EditText) dialogView.findViewById(R.id.edUsername);
        final EditText edPassword = (EditText) dialogView.findViewById(R.id.edPassword);
        final EditText edConfirmPass = (EditText) dialogView.findViewById(R.id.edConfirmPassword);
        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.imgClose);


        this.setView(dialogView)
                .setCancelable(false)
                .setPositiveButton(R.string.create_acc,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {

                            }
                        }
                )
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = this.create();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.hide();
            }
        });
        alert.show();
        // Настоящий обработчик для нажатия войти
        alert.getButton(alert.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( edUserName.getText().toString().isEmpty() ||
                        edPassword.getText().toString().isEmpty() ||
                        edConfirmPass.getText().toString().isEmpty()) {
                    Toast.makeText(
                            context,
                            R.string.enter_all_lines,
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    if (edPassword.getText().toString().equals(edConfirmPass.getText().toString())) {
                        try {
                            client.registerClient(
                                    edUserName.getText().toString(),
                                    edPassword.getText().toString()
                            );
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                        client.setConnectedCallback(new ConnectedCallBack() {
                            @Override
                            public void isConnectedCallback(boolean isConnected) {
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ChatFragment.client = client;
                                        Intent intent = new Intent(context, ChatActivity.class);
                                        intent.putExtra(Utils.SERVER, "Client");
                                        intent.putExtra("username", edUserName.getText().toString());
                                        context.startActivity(intent);
                                    }
                                });
                            }
                        });
                } else {
                        Toast.makeText(
                                context,
                                R.string.different_passwords,
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }
                alert.dismiss();
            }
        });
    }
}
