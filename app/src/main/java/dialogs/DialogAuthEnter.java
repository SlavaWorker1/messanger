package dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.slava.messanger.ChatActivity;
import com.example.slava.messanger.ChatFragment;
import com.example.slava.messanger.Client;
import com.example.slava.messanger.ConnectedCallBack;
import com.example.slava.messanger.R;
import com.example.slava.messanger.Utils;

import modelsdb.AuthorDb;

public class DialogAuthEnter extends AlertDialog.Builder  {

    /**
     * Создаём диалог для авторизации
     */
    public DialogAuthEnter(final Context context, int themeResId) {
        super(context, themeResId);

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_auth_enter, null);
        final EditText edUserName = (EditText) dialogView.findViewById(R.id.edUsername);
        final EditText edPassword = (EditText) dialogView.findViewById(R.id.edPassword);
        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.imgClose);

        this.setView(dialogView)
                .setCancelable(false)
                .setPositiveButton(R.string.enter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                /*
                                * Сдесь пусто, настоящий обработчик ниже.
                                * Сделано так, чтоб диалог не закрывался
                                * в случае некоретного логина/пароля
                                * сдесь dialog.dismiss() вызывается по умолчанию
                                * */
                            }
                        }
                )
                .setNegativeButton(R.string.create_acc, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new DialogRegister(context, R.style.MyAlertDialog);
                    }
                });

        final AlertDialog alert = this.create();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.hide();
            }
        });
        alert.show();
        // Настоящий обработчик для нажатия войти
        alert.getButton(alert.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edUserName.getText().toString().isEmpty()) {
                    Toast.makeText(
                            context,
                            R.string.enter_login_pass,
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
//                    проверяет есть ли в БД записи
                    if (!AuthorDb.getAuth(edUserName.getText().toString()).isEmpty()) {
                        // Достаёт настоящий пароль из БД
                        String authTruePassword = AuthorDb.getAuth(edUserName
                                        .getText()
                                        .toString()
                        ).get(0).authPass;
                        // Сверяет настоящий пароль с пароль из EditText
                        if (authTruePassword.equals(edPassword.getText().toString())) {
                            Intent intent = new Intent(context, ChatActivity.class);
                            intent.putExtra(
                                    Utils.SERVER,
                                    context.getString(R.string.Server)
                            );
                            intent.putExtra("username", edUserName.getText().toString());
                            context.startActivity(intent);
                            alert.dismiss();
                        } else {
                            Toast.makeText(
                                    context,
                                    R.string.invalid_login_pass,
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                }
            }
        });
    }


    /**
     * Перегруженный конструктор для авторизации клиента через сервер
     */
    public DialogAuthEnter(final Context context, int themeResId, final Client client) {
        super(context, themeResId);

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_auth_enter, null);
        final EditText edUserName = (EditText) dialogView.findViewById(R.id.edUsername);
        final EditText edPassword = (EditText) dialogView.findViewById(R.id.edPassword);
        final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.imgClose);

        this.setView(dialogView)
                .setCancelable(false)
                .setPositiveButton(R.string.enter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                /*
                                * Сдесь пусто, настоящий обработчик ниже.
                                * Сделано так, чтоб диалог не закрывался
                                * в случае некоретного логина/пароля
                                * сдесь dialog.dismiss() вызывается по умолчанию
                                * */
                            }
                        }
                )
                .setNegativeButton(R.string.create_acc, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new DialogRegister(context, R.style.MyAlertDialog, client);
                    }
                });

        final AlertDialog alert = this.create();
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.hide();
            }
        });
        alert.show();
        // Настоящий обработчик для нажатия войти
        alert.getButton(alert.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edUserName.getText().toString().isEmpty()) {
                    Toast.makeText(
                            context,
                            R.string.enter_login_pass,
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    client.authorizeClient(
                        edUserName.getText().toString(),
                        edPassword.getText().toString()
                    );
                    client.setConnectedCallback(new ConnectedCallBack() {
                        @Override
                        public void isConnectedCallback(boolean isConnected) {
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ChatFragment.client = client;
                                    Intent intent = new Intent(context, ChatActivity.class);
                                    intent.putExtra(Utils.SERVER, "Client");
                                    intent.putExtra("username", edUserName.getText().toString());
                                    context.startActivity(intent);
                                }
                            });
                        }
                    });
                }
                alert.dismiss();
            }
        });
    }
}
