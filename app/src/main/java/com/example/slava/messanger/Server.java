package com.example.slava.messanger;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import modelsdb.AuthorDb;


public class Server extends Connections {

    MessageCallBack messageCallBack;
    ServerSocket serverSocket;
    Socket socket;
    Context context;

    public Server(Handler handler, MessageCallBack messageCallBack, Context context) {
        this.mHandler = handler;
        this.messageCallBack = messageCallBack;
        this.context = context;
    }

    @Override
    void keepConnect() {
        try {
            serverSocket = new ServerSocket(Utils.PORT);
            Log.d("Server", "Waiting for a client");
            socket = serverSocket.accept();
            Log.d("Server", "Got a client");

            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            mDataInputStream = new DataInputStream(inputStream);
            mDataOutputStream = new DataOutputStream(outputStream);

            while (true) {
                incomeData = mDataInputStream.readUTF();
                Log.d("Server", "Client send: " + incomeData);

                JSONObject jsonObject = new JSONObject(incomeData);
                JSONObject data = jsonObject.getJSONObject("data");
                if (data.has("login")) {
                        authorizeClient(
                                data.getString("login"),
                                data.getString("password")
                        );
                } else if (data.has("newUser")) {
                    registerClient(
                            data.getString("newUser"),
                            data.getString("password")
                    );
                } else {
                    Log.d("Server", data.getString("text"));
                    mDataOutputStream.writeUTF(incomeData);
                    mDataOutputStream.flush();
                    messageCallBack.onNewMessageCallBack();
                    Message msg = mHandler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("Key", incomeData);
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                    addMessageToDb(jsonObject);
                }
            }

        }  catch (EOFException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            context,
                            "Собеседник покинул чат",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            });
            e.printStackTrace();
        }
        catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    void closeConnection() {
        try {
            serverSocket.close();
            socket.close();
            mDataInputStream.close();
            mDataOutputStream.flush();
            mDataOutputStream.close();
            this.interrupt();
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void authorizeClient(String username, String password) throws JSONException, IOException {
        JSONObject jsonMessage = new JSONObject();
        JSONObject jsonData = new JSONObject();
        Log.d("Server", "Попытка авторизации");
            if (!AuthorDb.getAuth(username).isEmpty()) {
                // Достаёт настоящий пароль из БД
                String authTruePassword = AuthorDb.getAuth(username).get(0).authPass;
                jsonMessage.put("id", System.currentTimeMillis() / 100);
                jsonMessage.put("data", jsonData);
                // Сверяет настоящий пароль с пароль из EditText
                if (authTruePassword.equals(password)) {
                    jsonData.put("autorize", "success");
                } else {
                    jsonData.put("autorize", "failed");
                }
                mDataOutputStream.writeUTF(jsonMessage.toString());
                mDataOutputStream.flush();
            }
    }

    @Override
    void registerClient(String username, String password) throws JSONException, IOException {
        Log.d("Server", "Попытка регистрации");
        AuthorDb authorDb = new AuthorDb();
        authorDb.authName = username;
        authorDb.authPass = password;
        authorDb.save();

        JSONObject jsonMessage = new JSONObject();
        JSONObject jsonData = new JSONObject();
        jsonMessage.put("id", System.currentTimeMillis() / 100);
        jsonMessage.put("data", jsonData);
        jsonData.put("register", "success");
        mDataOutputStream.writeUTF(jsonMessage.toString());
        mDataOutputStream.flush();
    }
}
