package com.example.slava.messanger;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import modelsdb.MessangeDb;


public class ChatFragment extends Fragment implements View.OnClickListener, MessageCallBack, OnBackPressedListener {

    private EditText edMessage;
    public  static Client client;
    private Server server = null;
    private RecycleViewAdapter recycleViewAdapter;
    private DateFormat mDateFormat = new SimpleDateFormat("HH:mm:ss");
    private List<Message> messageList;
    private RecyclerView recyclerView;
    private String username;


    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ((ChatActivity)getActivity()).setOnBackPressedListener(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView  =  inflater.inflate(R.layout.fragment_chat, container, false);

        messageList = new ArrayList<>();
        username = getActivity().getIntent().getStringExtra("username");

//        Загружаеm сообщения с Базы данных;
        List<MessangeDb> messangeDbList = MessangeDb.getAll();

        for(int i = 0; i < messangeDbList.size(); i++ ) {
            messageList.add(new Message(
                messangeDbList.get(i).author_name,
                messangeDbList.get(i).messanges,
                messangeDbList.get(i).date,
                messangeDbList.get(i).isServerMessange
            ));
        }

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycleViewAdapter = new RecycleViewAdapter(messageList);
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(llm);
        recyclerView.scrollToPosition(messageList.size() - 1);
        recyclerView.setAdapter(recycleViewAdapter);

        edMessage = (EditText)rootView.findViewById(R.id.edMessageText);
        ImageView imgSendMessage = (ImageView) rootView.findViewById(R.id.imgSendMessage);
        imgSendMessage.setOnClickListener(this);

        MessagesHandler messagesHandler = new MessagesHandler(this);

        if (isServiceDevice()) {
            server = new Server(messagesHandler, this, getActivity());
            server.start();
        } else {
            client.setHandler(messagesHandler);
            client.setMessageCallBack(this);
            client.setContext(getActivity());
        }

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSendMessage:
                try {
                    String message = edMessage.getText().toString();
                    if (!message.isEmpty()) {

                        recyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
                            }
                        }, 250);

                        if (client != null) {
                            client.sendMessage(message, username, 0);
                        } else {
                            server.sendMessage(message, username, 1);
                            recycleViewAdapter.getMessages().add(new Message(
                                    username,
                                    message,
                                    mDateFormat.format(new Date()),
                                    1
                            ));
                            recycleViewAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(
                                getActivity(),
                                R.string.enter_message,
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                    break;
                } catch (NullPointerException e) {
                    Toast.makeText(
                            getActivity(),
                            R.string.cant_send_message,
                            Toast.LENGTH_SHORT
                    ).show();
                }
                finally {
                    edMessage.setText("");
                }
        }
    }


    // Скролит recycleView вниз при получении нового сообщения
    @Override
    public void onNewMessageCallBack() {
        recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                recyclerView
                        .smoothScrollToPosition(recyclerView
                                .getAdapter()
                                .getItemCount() - 1);
            }
        }, 250);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                    if (isServiceDevice()) {
                        server.closeConnection();
                    } else {
                        client.closeConnection();
                    }
                return true;
            default:
                return onOptionsItemSelected(item);
        }
    }

//    Вызывает деструтор при выходе из чата
    @Override
    public void onBackPressed() {
        if (isServiceDevice()) {
            server.closeConnection();
        } else {
            client.closeConnection();
        }
    }


    /**
     * Handler для добавления карточек в RecycleView без утечек памяти
     */
   static class MessagesHandler extends Handler {
        private JSONObject jsonObject;
        private WeakReference<ChatFragment> weakReference;

        public MessagesHandler(ChatFragment weakReference) {
            this.weakReference =  new WeakReference<>(weakReference);

        }

        @Override
        public void handleMessage(android.os.Message msg) {
            Bundle bundle = msg.getData();
            String date = bundle.getString("Key");
            try {
                Fragment fragment = weakReference.get();
                if (fragment != null) {
                    jsonObject = new JSONObject(date);
                    ((ChatFragment) fragment).recycleViewAdapter.getMessages().add(new Message(
                            jsonObject.getJSONObject("data").getString("username"),
                            jsonObject.getJSONObject("data").getString("text"),
                            jsonObject.getJSONObject("data").getString("time"),
                            jsonObject.getJSONObject("data").getInt("isServer")
                    ));
                    ((ChatFragment) fragment).recycleViewAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.handleMessage(msg);
        }
    }

    private boolean isServiceDevice() {
        if (getActivity().getIntent().getStringExtra(Utils.SERVER).equals("Server")) {
            return true;
        } else {
            return false;
        }
    }

}
