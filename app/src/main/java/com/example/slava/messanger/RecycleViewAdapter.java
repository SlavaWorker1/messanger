package com.example.slava.messanger;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;


public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.MessageHolder> {

    private List<Message> messages;

    public List<Message> getMessages() {
        return messages;
    }

    public RecycleViewAdapter(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.card_message_layout,
                parent,
                false
        );
        return new MessageHolder(v);
    }


    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        Message message = messages.get(position);
        holder.cardView.setRadius(50);
        holder.tvName.setText(message.getAuthor());
        holder.tvName.setTextColor(message.isIsServerMessage() ? Color.RED : Color.BLACK);
        ((RelativeLayout) holder.itemView).setGravity(message.isIsServerMessage() ? Gravity.RIGHT : Gravity.LEFT);
        holder.tvMessage.setText(message.getMessage());
        holder.tvDate.setText(message.getDate());

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class MessageHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private TextView tvMessage;
        private TextView tvDate;
        private CardView cardView;

        public MessageHolder(View itemView) {
            super(itemView);
            cardView = (CardView)itemView.findViewById(R.id.cardView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        }
    }
}
