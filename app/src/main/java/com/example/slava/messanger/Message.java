package com.example.slava.messanger;


public class Message {

    private String author;
    private String message;
    private String date;
    private int isServerMessange;

    public Message(String author, String message, String date, int isServerMessange) {
        this.author = author;
        this.message = message;
        this.date = date;
        this.isServerMessange = isServerMessange;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public boolean isIsServerMessage (){
        return isServerMessange == 1;
    }
}
