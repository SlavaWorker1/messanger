package com.example.slava.messanger;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import dialogs.DialogAuthEnter;
import dialogs.DialogIpEnter;

public class MainFragment extends Fragment implements View.OnClickListener {

    private Button btnServer, btnClient;
    private TextView tvIpAddress;


    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "PoetsenOne-Regular.ttf");
        TextView tvTitle = (TextView)rootView.findViewById(R.id.tvTitle);
        tvTitle.setTypeface(type);

        btnClient = (Button)rootView.findViewById(R.id.btnClient);
        btnClient.setOnClickListener(this);
        btnServer = (Button)rootView.findViewById(R.id.btnServer);
        btnServer.setOnClickListener(this);
        tvIpAddress = (TextView)rootView.findViewById(R.id.tvMyIp);
        tvIpAddress.setText("Ваш IP: " + Utils.getIPAddress());

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClient:
                new DialogIpEnter(getActivity(), R.style.MyAlertDialog);
                break;
            case R.id.btnServer:
                new DialogAuthEnter(getActivity(), R.style.MyAlertDialog);
                break;
        }
    }


}
