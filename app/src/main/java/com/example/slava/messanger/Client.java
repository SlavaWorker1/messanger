package com.example.slava.messanger;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;


public class Client extends Connections {

    private String mIpAddress;
    private Socket socket;
    ConnectedCallBack connectedCallBack;
    MessageCallBack messageCallBack;
    Context context;

    public Client(String mIpAddress) {
        this.mIpAddress = mIpAddress;
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setConnectedCallback(ConnectedCallBack connectedCallBack){
        this.connectedCallBack = connectedCallBack;
    }

    public void setMessageCallBack(MessageCallBack messageCallBack) {
        this.messageCallBack = messageCallBack;
    }

    @Override
    void keepConnect() {
        int mServerPort = 6060;

        try {
            InetAddress ipAddress = InetAddress.getByName(mIpAddress);
            Log.d("Client", "Socket with ip: " + mIpAddress + " port: " + mServerPort);
            socket = new Socket(ipAddress, mServerPort);

            connectedCallBack.isConnectedCallback(true);

            Log.d("Client", "Connected");

            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            mDataInputStream = new DataInputStream(inputStream);
            mDataOutputStream = new DataOutputStream(outputStream);


            while (true) {
                incomeData = mDataInputStream.readUTF();
                JSONObject jsonObject = new JSONObject(incomeData);
                JSONObject data = jsonObject.getJSONObject("data");
                if (data.has("autorize")) {
                    Log.d("Client", "Autorize is :" + data.getString("autorize"));
                    if (data.getString("autorize").equals("success")) {
                        connectedCallBack.isConnectedCallback(true);
                    }
                } else if (data.has("register")) {
                        Log.d("Client", "Register is :" + data.getString("register"));
                        if(data.getString("register").equals("success")) {
                            connectedCallBack.isConnectedCallback(true);
                        }
                } else {
                    Log.d("Client", data.getString("text"));
                    messageCallBack.onNewMessageCallBack();
                    Message msg = mHandler.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("Key", incomeData);
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                    addMessageToDb(jsonObject);
                }
            }

        } catch (EOFException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(
                            context,
                            R.string.lost_connection,
                            Toast.LENGTH_SHORT
                    ).show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    void closeConnection() {
        try {
            socket.close();
            mDataInputStream.close();
            mDataOutputStream.flush();
            mDataOutputStream.close();
            this.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
     public void authorizeClient (String login, String password) {
        JSONObject jsonMessage = new JSONObject();
        JSONObject jsonData = new JSONObject();
        try {
            jsonMessage.put("id", System.currentTimeMillis() / 100);
            jsonMessage.put("data", jsonData);
            jsonData.put("login", login);
            jsonData.put("password", password);
            mDataOutputStream.writeUTF(jsonMessage.toString());
            mDataOutputStream.flush();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registerClient(String username, String password) throws JSONException, IOException {
        JSONObject jsonMessage = new JSONObject();
        JSONObject jsonData = new JSONObject();
        try {
            jsonMessage.put("id", System.currentTimeMillis() / 100);
            jsonMessage.put("data", jsonData);
            jsonData.put("newUser", username);
            jsonData.put("password", password);
            mDataOutputStream.writeUTF(jsonMessage.toString());
            mDataOutputStream.flush();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

}
