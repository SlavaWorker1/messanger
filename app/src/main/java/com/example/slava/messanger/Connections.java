package com.example.slava.messanger;

import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import modelsdb.MessangeDb;


public abstract class Connections extends Thread {

    DataOutputStream mDataOutputStream;
    DataInputStream mDataInputStream;
    Handler mHandler;
    String incomeData = null;
    DateFormat mDateFormat = new SimpleDateFormat("HH:mm:ss");


    @Override
    public void run() {
        keepConnect();
        super.run();
    }

    abstract void keepConnect();

    abstract void closeConnection();

    abstract void authorizeClient(String username, String password) throws JSONException, IOException;

    abstract void registerClient(String username, String password) throws  JSONException, IOException;

    public void sendMessage(String message, String username, int isServer) {
        JSONObject jsonMessage = new JSONObject();
        JSONObject jsonData = new JSONObject();
        Date date = new Date();
        try {
            jsonMessage.put("id", System.currentTimeMillis()/100);
            jsonMessage.put("data", jsonData);
            jsonData.put("username", username);
            jsonData.put("text", message);
            jsonData.put("time", mDateFormat.format(date));
            jsonData.put("isServer", isServer);
            mDataOutputStream.writeUTF(jsonMessage.toString());
            mDataOutputStream.flush();

            addMessageToDb(jsonMessage);

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    // Парсит Json и добавляет сообщения в БД
   public void addMessageToDb(JSONObject jsonMessage) throws JSONException {
       JSONObject data = jsonMessage.getJSONObject("data");
       Log.d("Client", data.getString("text"));
       new MessangeDb()
           .setIsServerMessange(data.getInt("isServer"))
           .setAuthorName(data.getString("username"))
           .setMessanges(data.getString("text"))
           .setDate(data.getString("time"))
           .save();
    }
}
